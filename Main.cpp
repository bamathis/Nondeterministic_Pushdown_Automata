//Name: Brandon Mathis
//File Name: Main.cpp
//Date: 8 May, 2015
//Program Description: Driver file for the NPDA class

#include <iostream>
#include<fstream>
#include <stdlib.h>
#include<string.h>
#include "NPDA.h"
using namespace std;

void setupNPDA(NPDA& automata, string automataInfo, int& section);

int main (int argc, char* argv[])
    {
    ifstream inFile;
    string automataInfo;
    bool accepted;
    int section;
    string inputString;
    NPDA automata;
    int lineSize;

    if(argc < 2)                    //If only the program name was given
        {
        cout << "usage: <prog_name> <file_name>" << endl;
        cout << endl;
        exit(1);
        }
    inFile.open(argv[1]);
    if (inFile.fail())               //If the NPDA file was not found
        {
        cout << "NPDA file could not be opened!" << endl;
        cout << endl;
        exit(1);
        }
    section = 0;            //Keeps track of what section of NPDA should be populated
    while(getline(inFile,automataInfo))
        setupNPDA(automata,automataInfo,section);

    automata.displayNPDA();                 //Displays NPDA description
    cout << endl;
    inFile.close();
    while(true)
        {
        automata.resetNPDA();               //Sets the NPDA back to initial conditions
        cout << "Enter a string (CTRL^C to end): ";
        getline(cin,inputString);
        cout << endl;
        accepted = automata.transition(inputString,automata.getInitialState(),automata.getStackStartSymbol());  //Processes input string and returns whether it is accepeted
        if(accepted)
            {
            automata.displayTransitionList();
            cout << "Accepted" << endl;
            }
        else
            cout << "Rejected" << endl;
        cout << endl;
        }
    }
//====================================================================
//Adds all states, alphabet symbols, and transitions to the NPDA
void setupNPDA(NPDA& automata,string automataInfo, int& section)
    {
    int n;
    n = automataInfo.length() - 1;
    if(automataInfo[n] == 13)
        automataInfo = automataInfo.substr(0,automataInfo.length()-1);
    if(automataInfo.compare("<states>") == 0)               //Found the header for NPDA states
        section++;
    else if(automataInfo.compare("<input alphabet>") == 0)  //Found the header for alphabet of NPDA
        section++;
    else if(automataInfo.compare("<stack alphabet>") == 0)  //Found the header for stack alphabet for NPDA
        section++;
    else if(automataInfo.compare("<transitions>") == 0)     //Found the header for transitions for NPDA
        section++;
    else if(automataInfo.compare("<initial state>") == 0)   //Found the header for initial state of NPDA
        section++;
    else if(automataInfo.compare("<stack start>") == 0)     //Found the header for stack start for NPDA
        section++;
    else if(automataInfo.compare("<final states>") == 0)    //Found the header for final states of NPDA
        section++;
    else                                                    //Currently in a section of the file that populates NPDA
        {
        if(section == 1)
            automata.addStates(automataInfo);
        else if(section == 2)
            automata.addToAlphabet(automataInfo);
        else if(section == 3)
            automata.addToStackAlphabet(automataInfo);
        else if(section == 4)
            {
            int stringNum;
            string state;
            string transition;
            string stackSymbol;
            string transitionState;
            string newStackSymbol;
            int length;
            stringNum = 0;
            state = "";
            transition = "";
            transitionState = "";
            length = automataInfo.size();
            for (int i = 0; i < length; ++i)
                {
                if(stringNum == 0)                          //If still part of the current state
                    {
                    if(automataInfo[i] != ' ')              //If the position being appended isn't a space
                        state = state + automataInfo[i];
                    else
                        stringNum++;
                    }
                else if(stringNum == 1)                     //If still part of the input symbol
                    {
                    if(automataInfo[i] != ' ')              //If the position being appended isn't a space
                        transition = transition + automataInfo[i];
                    else
                        stringNum++;
                    }
                else if(stringNum == 2)                     //If still part of the stack top
                    {
                    if(automataInfo[i] != ' ')              //If the position being appended isn't a space
                        stackSymbol = stackSymbol + automataInfo[i];
                    else
                        stringNum++;
                    }
                else if(stringNum == 3)                     //If still part of the state being transitioned to
                    {
                    if(automataInfo[i] != ' ')              //If the position being appended isn't a space
                        transitionState = transitionState + automataInfo[i];
                    else
                        stringNum++;
                    }
                else if(stringNum == 4)                     //If still part of the new stack symbol
                    {
                    if(automataInfo[i] != ' ')              //If the position being appended isn't a space
                        newStackSymbol = newStackSymbol + automataInfo[i];
                    else
                        break;
                    }
                }
            automata.addTransition(state,transition,stackSymbol,transitionState,newStackSymbol);
            }
        else if(section == 5)
                automata.setInitialState(automataInfo);
        else if(section == 6)
                automata.setStackStartSymbol(automataInfo);
        else if(section == 7)
                automata.addFinalStates(automataInfo);
        }
    }