//Name: Brandon Mathis
//File Name: NPDA.h
//Date: 8 May, 2015
//Program Description: Header file for NPDA

#include <vector>
#include <string>
#include <map>
#include <utility>
using namespace std;

class NPDA
	{
	private:
		vector<string> states;
		vector<string> alphabet;
		vector<string> finalStates;
		vector<string> stackAlphabet;
		string initialState;
		string currentState;
		string stackStartSymbol;
		map<tuple<string,string,string>,vector<pair<string,string>>> transitionList;
		vector<tuple<string,string,string>> transitionsMade;

	public:
		NPDA();
		void addStates(string state);
		void addToAlphabet(string letter);
		void addFinalStates(string state);
		void addToStackAlphabet(string letter);
		void setStackStartSymbol(string letter);
		string getStackStartSymbol();
		void setInitialState(string state);
		string getInitialState();
		void resetNPDA();
		void resetTransitions();
		void addTransition(string inputState, string transition,string stackSymbol, string transitionState, string newStackSymbol);
		bool transition(string currentInput,string state, string currentStackSymbol); 
		bool checkFinalState(string state);
		void displayNPDA();
		void displayTransitionList();
	};
#include "NPDA.cpp"
