//Name: Brandon Mathis
//File Name: NPDA.cpp
//Date: 8 May, 2015
//Program Description: Function definitions for NPDA

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <utility>
using namespace std;

//===============================================================
//Default constructor for NPDA class
NPDA::NPDA()
	{
	initialState = "";
	currentState = "";
	}
//===============================================================
//Add a symbol to the alphabet
void NPDA::addToAlphabet(string letter)
	{
	alphabet.insert(alphabet.end(),letter);	//Insert new symbol at end of vector
	}
//===============================================================
//Add a state to the NPDA
void NPDA::addStates(string state)
	{
	states.insert(states.end(),state);	//Insert new state at end of vector
	}
//===============================================================
//Add a final state to the NPDA
void NPDA::addFinalStates(string state)
	{
	finalStates.insert(finalStates.end(),state);	//Insert new final state at end of vector	
	}
//===============================================================
//Add a symbol to the stack alphabet
void NPDA::addToStackAlphabet(string letter)
	{
	stackAlphabet.insert(stackAlphabet.end(),letter);	//Insert new symbol at end of vector
	}
//===============================================================
//Set stack start symbol of the NPDA
void NPDA::setStackStartSymbol(string symbol)
	{
	stackStartSymbol = symbol;
	}
//===============================================================
//Set initial state and current state of the NPDA
void NPDA::setInitialState(string state)
	{
	initialState = state;
	currentState = state;
	}
//===============================================================
//Returns stack start symbol of the NPDA
string NPDA::getStackStartSymbol()
	{
	return stackStartSymbol;
	}
//==============================================================
//Returns the initial state of the NPDA
string NPDA::getInitialState()
	{
	return initialState;	
	}
//==============================================================
//Resets the NPDA to initial conditions
void NPDA::resetNPDA()
	{
	currentState = initialState;	
	transitionsMade.clear();										//Empty vector of previous transitions
	}
//===============================================================
//Adds a new transition to the NPDA
void NPDA::addTransition(string inputState, string transition,string stackSymbol, string transitionState, string newStackSymbol)
	{
	tuple<string,string,string> state(inputState,transition,stackSymbol);	//Creates a tuple containing the input state and the transition it makes
	pair<string,string> newState(transitionState,newStackSymbol);
	transitionList[state].insert(transitionList[state].end(),newState);		//Adds the transition to the Map of transitions
	}
//===============================================================
//Transitions from current state to new state on the input symbol
bool NPDA::transition(string currentInput,string state, string currentStackSymbol) 
	{
	string newInput;
	string newStackSymbol;
	bool accepted;
	int length;
	accepted = false;
	try
		{
		if(currentInput.compare("") == 0)
			currentInput = "*";
		if(checkFinalState(state) && currentInput.compare("*") == 0)				//String is accepted
			{
			if(currentStackSymbol.compare("") == 0)
					currentStackSymbol = "*";
			tuple<string,string,string> transitionToDisplay(state,currentInput,currentStackSymbol);	
			transitionsMade.insert(transitionsMade.end(),transitionToDisplay);			//Store last transition
			return true;	
			}
		else
			{	
			newInput = currentInput.substr(0,1);									//Gets next value of the input string
			newStackSymbol = currentStackSymbol.substr(0,1);						//Gets the stack top
			tuple<string,string,string> tempState(state,newInput,newStackSymbol);	
			vector<pair<string,string>> listOfStates;
			listOfStates = transitionList.at(tempState);							//Gets vector of possible transitions
			length = listOfStates.size();											//Number of possible paths to acceptance at current level
			for(int n = 0; n < length && !accepted; n++)
				{
				pair<string,string> toState;
				tuple<string,string,string> transitionToDisplay(state,currentInput,currentStackSymbol);
				transitionsMade.insert(transitionsMade.end(),transitionToDisplay);	//Add the current transition as possible path to acceptance
				toState = listOfStates[n];
				state = toState.first;
				if(toState.second.compare("*") == 0)								//Pop stack top for lambda 
					currentStackSymbol = currentStackSymbol.substr(1,currentStackSymbol.size());
				else																//Remove previous stack top and add new top
					currentStackSymbol = toState.second + currentStackSymbol.substr(1,currentStackSymbol.size());
				currentInput = currentInput.substr(1,currentInput.size());			//Remove first value of input string
				accepted = accepted || transition(currentInput,state,currentStackSymbol);	//True if there is a path to acceptance
				if(!accepted)
					transitionsMade.erase(--transitionsMade.end());					//Remove invalid paths
				}
			}
		}
	catch(...)																		//There was no transition to next state
		{
		return false;
		}
	return accepted;
	}
//===============================================================
//Returns whether the NPDA is in a final state or not
bool NPDA::checkFinalState(string state)
	{
	int size;
	bool found;
	found = false;
	size = finalStates.size();
	for(int n = 0; n < size && !found; n++)
		{
		if(state.compare(finalStates[n]) == 0)		//If the current state of the NPDA is a final state
			found = true;	
		}
	return found;
	}
//============================================================
//Displays all the information contained in the NPDA
void NPDA::displayNPDA()
	{
	int size;
	cout << "----------N P D A----------" << endl;
	cout << "<states>" << endl;
	size = states.size();
	for(int n = 0; n < size; n++)
		cout << states[n] << " ";
	cout << endl;
	cout << "<alphabet>" << endl;
	size = alphabet.size();
	for(int n = 0; n < size; n++)
		cout << alphabet[n] << " ";
	cout << endl;
	cout << "<stack alphabet>" << endl;
	for(int n = 0; n < size; n++)
		cout << stackAlphabet[n] << " ";
	cout << endl;
	cout << "<transitions>" << endl;
	for(map<tuple<string,string,string>,vector<pair<string,string>>>::iterator i = transitionList.begin(); i != transitionList.end(); i++)	//All transitions stored in transitionList
		{
		size = (i -> second).size();
		for(int n = 0; n < size; n++)
			{
			cout << "(" << get<0>(i -> first) << "," << get<1>(i -> first)<< "," << get<2>(i -> first) << ") -> " ;					//(Start state, input symbol, stack symbol)
			cout << "(" << (i -> second).at(n).first << "," << (i -> second).at(n).second << ")" << endl;							//(New state, new stack symbol)
			}
		}
	cout << "<initial state>" << endl;
	cout << initialState << endl;
	cout << "<stack start>" <<endl;
	cout << stackStartSymbol << endl;
	cout << "<final states" << endl;
	size = finalStates.size();
	for(int n = 0; n < size; n++)
		cout << finalStates[n] << " ";	
	cout << endl;
	cout <<"-------------------------" << endl;
	}
//==============================================================
//Diplays all transitions to final state
void NPDA::displayTransitionList()
	{
	int length;
	length = transitionsMade.size();
	cout << "(" << get<0>(transitionsMade[0]) << "," << get<1>(transitionsMade[0]);
	cout << ","<< get<2>(transitionsMade[0]) << ")" << endl;
	for(int n = 1; n < length; n++)
		{
		cout << "|- (" << get<0>(transitionsMade[n]) << "," << get<1>(transitionsMade[n]);
		cout << ","<< get<2>(transitionsMade[n]) << ")" << endl;			
		}
	}