# Nondeterministic_Pushdown_Automata

[Nondeterministic_Pushdown_Automata](https://gitlab.com/bamathis/Nondeterministic_Pushdown_Automata) is an implementation of a Nondeterministic Pushdown Automata that determines whether a string is part of the language.

## Quick Start

### Program Execution

```
$ ./Main.cpp filename
```

### File Input

Input file for the automata should be in the form of the included file **test.txt**.